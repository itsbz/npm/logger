const
    app = require('express')(),
    logger = require('./index'),
    path = require('path'),
    app_info = {name:'test', version:'1.0.0'}

logger(app, {
    app_name: app_info.name,
    app_version: app_info.version,
    access_log: path.join(__dirname, 'logs/access.log'),
    info_log: path.join(__dirname, 'logs/info.log'),
    error_log: path.join(__dirname, 'logs/error.log'),
    fatal: path.join(__dirname, 'logs/fatal.log')
})

console.debug('Test DEBUG')
console.log('Test LOG')
console.warn('Test WARN')
console.error('Test ERROR')

