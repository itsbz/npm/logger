# ITS.bz :: Logger

Universal logger for NodeJS projects

## Installation

```
yarn add itsbz-logger
```

or

```
npm install itsbz-logger
```

## Usage

```javascript
const
app = require('express')(),
Telegraf = require('telegraf'),
logger = require('itsbz-logger'),
path = require('path'),
app_info = require('./package.json'),
bot = new Telegraf(settings.telegram.token, tg_settings);

logger(app, {
app_name: app_info.name,
app_version: app_info.version,
access_log: path.join(__dirname, 'logs/access.log'),
info_log: path.join(__dirname, 'logs/info.log'),
error_log: path.join(__dirname, 'logs/error.log'),
fatal: path.join(__dirname, 'logs/fatal.log'),
bot: bot,
chat: '-1234567890'
})

console.log('Test LOG')
console.debug('Test DEBUG')
console.warn('Test WARN')
console.error('Test ERROR')
```

or without Telegram bot:
```javascript
const
app = require('express')(),
logger = require('itsbz-logger'),
path = require('path'),
app_info = require('./package.json')

logger(app, {
app_name: app_info.name,
app_version: app_info.version,
access_log: path.join(__dirname, 'logs/access.log'),
info_log: path.join(__dirname, 'logs/info.log'),
error_log: path.join(__dirname, 'logs/error.log'),
fatal: path.join(__dirname, 'logs/fatal.log')
})

console.log('Test LOG')
console.debug('Test DEBUG')
console.warn('Test WARN')
console.error('Test ERROR')
```

## Output sample
At the console:\
![screen_console](https://gitlab.com/its.bz/npm/logger/-/raw/master/screen_console.png)

Log files:\
![screen_files](https://gitlab.com/its.bz/npm/logger/-/raw/master/screen_files.png)

## Contributing

0. Create project directory: `mkdir itsbz-logger && cd itsbz-logger`
1. Clone repo: `git clone https://gitlab.com/its.bz/npm/logger.git ./`
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History
```
v1.0.4 @ 20 November 2020  
- Fix README screenshots

v1.0.3 @ 20 November 2020  
- Add screenshots, auto-changelog, fixed log.error output

v1.0.2 @ 19 November 2020  
- Add test.js file, run commands for IDE, update README.md

v1.0.1 @ 19 November 2020  
- First working package  
- Initial commit  
- Cleanup package

```

## Credits

Contact us: [dev@its.bz](mailto:dev@its.bz)\
Our site: [its.bz](https://its.bz)

## License

GPL
